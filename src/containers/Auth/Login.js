import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from "connected-react-router";
import * as actions from "../../store/actions";
import { Link } from 'react-router-dom';
import handleLogin from "../../services/index";

import './Login.scss';
import {FormattedMessage } from 'react-intl';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username:"",
            password:"",
            hidden : false,
        }
    }

    componentDidMount() {
        this.interval = setInterval(() => this.handleTimeHiddenPassword(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    handleChangeUsername = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    handleChangePassowrd = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    handleLogin = async () => {
        console.log('All : ' , this.state);
        await this.handleLogin(this.state.username, this.state.password);
    }

    handleChangeHiddenPassword = () => {
        this.setState({
            hidden: !this.state.hidden,
        });
    }
    
    handleTimeHiddenPassword = () => {
        this.setState({
            hidden : false,
        });
    }


    render() {
        return (
            <div className="login-wrapper">
                <div className="login-container">
                    <div className="login-content row">
                        <div className="col-12 text-center login-title">
                            <p>Login</p>
                        </div>
                        <div className="col-12 form-group">
                            <input type="text" 
                            className="form-control" 
                            placeholder= "Enter Your Username"
                            value = {this.props.username}
                            onChange = {(event) => this.handleChangeUsername(event)}
                            />
                        </div>
                        <div className="col-12 form-group">
                            <div className="login-form-password">
                                <input type={this.state.hidden ? 'text' : 'password'}
                                className="form-control" 
                                placeholder= "Enter Your Password" 
                                onChange = {(event) => this.handleChangePassowrd(event)}
                                />
                                <span onClick={() => this.handleChangeHiddenPassword()}>
                                    <i className={this.state.hidden ? "fas fa-eye" : "fas fa-eye-slash"}></i>
                                </span>
                            </div>
                        </div>
                        <div className="col-12 form-group login-button">
                            <button onClick={() => this.handleLogin()}>Login</button>
                        </div>
                        <div className="mt-3"><p className="text-center">Or Sign in With</p></div>
                        <div className="col-12 form-group login-provider">
                            <button className="login-facebook"><i className="fab fa-facebook"></i></button>
                            <button className="login-twitter"><i className="fab fa-twitter"></i></button>
                            <button className="login-google"><i className="fab fa-google"></i></button>
                        </div>
                        <div className="col-12 form-group row">
                            <Link path="/" className="col-6 login-forgot text-center">Forgot Password</Link>
                            <Link path="/" className="col-6 login-forgot text-center">Register</Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        lang: state.app.language
    };
};

const mapDispatchToProps = dispatch => {
    return {
        navigate: (path) => dispatch(push(path)),
        adminLoginSuccess: (adminInfo) => dispatch(actions.adminLoginSuccess(adminInfo)),
        adminLoginFail: () => dispatch(actions.adminLoginFail()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
